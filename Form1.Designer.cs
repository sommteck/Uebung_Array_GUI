namespace Übung_Array_GUI
{
    partial class Uebung_Array
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmd_end = new System.Windows.Forms.Button();
            this.cmd_clear = new System.Windows.Forms.Button();
            this.cmd_Int10 = new System.Windows.Forms.Button();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.cmd_double_array_mit_5_Elementen = new System.Windows.Forms.Button();
            this.cmd_string_arrays_3_Elementen = new System.Windows.Forms.Button();
            this.cmd_string_splitten = new System.Windows.Forms.Button();
            this.cmd_char_array = new System.Windows.Forms.Button();
            this.cmd_char_array_rueckwaerts = new System.Windows.Forms.Button();
            this.cmd_array_verweis = new System.Windows.Forms.Button();
            this.cmd_array_copy = new System.Windows.Forms.Button();
            this.cmd_2d_array = new System.Windows.Forms.Button();
            this.cmd_1_Zeichen_aus_2darray = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // cmd_end
            // 
            this.cmd_end.Location = new System.Drawing.Point(454, 411);
            this.cmd_end.Name = "cmd_end";
            this.cmd_end.Size = new System.Drawing.Size(75, 41);
            this.cmd_end.TabIndex = 0;
            this.cmd_end.Text = "&Ende";
            this.cmd_end.UseVisualStyleBackColor = true;
            this.cmd_end.Click += new System.EventHandler(this.cmd_end_Click);
            // 
            // cmd_clear
            // 
            this.cmd_clear.Location = new System.Drawing.Point(304, 411);
            this.cmd_clear.Name = "cmd_clear";
            this.cmd_clear.Size = new System.Drawing.Size(92, 41);
            this.cmd_clear.TabIndex = 1;
            this.cmd_clear.Text = "Textbox &Löschen";
            this.cmd_clear.UseVisualStyleBackColor = true;
            this.cmd_clear.Click += new System.EventHandler(this.cmd_clear_Click);
            // 
            // cmd_Int10
            // 
            this.cmd_Int10.Location = new System.Drawing.Point(12, 12);
            this.cmd_Int10.Name = "cmd_Int10";
            this.cmd_Int10.Size = new System.Drawing.Size(108, 47);
            this.cmd_Int10.TabIndex = 2;
            this.cmd_Int10.Text = "Int-Array mit 10 Elementen";
            this.cmd_Int10.UseVisualStyleBackColor = true;
            this.cmd_Int10.Click += new System.EventHandler(this.cmd_Int10_Click);
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(304, 12);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ReadOnly = true;
            this.richTextBox1.Size = new System.Drawing.Size(324, 332);
            this.richTextBox1.TabIndex = 3;
            this.richTextBox1.Text = "";
            // 
            // cmd_double_array_mit_5_Elementen
            // 
            this.cmd_double_array_mit_5_Elementen.Location = new System.Drawing.Point(12, 81);
            this.cmd_double_array_mit_5_Elementen.Name = "cmd_double_array_mit_5_Elementen";
            this.cmd_double_array_mit_5_Elementen.Size = new System.Drawing.Size(108, 46);
            this.cmd_double_array_mit_5_Elementen.TabIndex = 4;
            this.cmd_double_array_mit_5_Elementen.Text = "Initialisierter Double-Array mit 5 Elementen";
            this.cmd_double_array_mit_5_Elementen.UseVisualStyleBackColor = true;
            this.cmd_double_array_mit_5_Elementen.Click += new System.EventHandler(this.cmd_double_array_mit_5_Elementen_Click);
            // 
            // cmd_string_arrays_3_Elementen
            // 
            this.cmd_string_arrays_3_Elementen.Location = new System.Drawing.Point(12, 156);
            this.cmd_string_arrays_3_Elementen.Name = "cmd_string_arrays_3_Elementen";
            this.cmd_string_arrays_3_Elementen.Size = new System.Drawing.Size(108, 74);
            this.cmd_string_arrays_3_Elementen.TabIndex = 5;
            this.cmd_string_arrays_3_Elementen.Text = "Initialisieren eines String-Arrays mit 3 Elementen";
            this.cmd_string_arrays_3_Elementen.UseVisualStyleBackColor = true;
            this.cmd_string_arrays_3_Elementen.Click += new System.EventHandler(this.cmd_string_arrays_3_Elementen_Click);
            // 
            // cmd_string_splitten
            // 
            this.cmd_string_splitten.Location = new System.Drawing.Point(12, 253);
            this.cmd_string_splitten.Name = "cmd_string_splitten";
            this.cmd_string_splitten.Size = new System.Drawing.Size(108, 52);
            this.cmd_string_splitten.TabIndex = 6;
            this.cmd_string_splitten.Text = "String splitten";
            this.cmd_string_splitten.UseVisualStyleBackColor = true;
            this.cmd_string_splitten.Click += new System.EventHandler(this.cmd_string_splitten_Click);
            // 
            // cmd_char_array
            // 
            this.cmd_char_array.Location = new System.Drawing.Point(13, 329);
            this.cmd_char_array.Name = "cmd_char_array";
            this.cmd_char_array.Size = new System.Drawing.Size(107, 44);
            this.cmd_char_array.TabIndex = 7;
            this.cmd_char_array.Text = "Char-Array";
            this.cmd_char_array.UseVisualStyleBackColor = true;
            this.cmd_char_array.Click += new System.EventHandler(this.cmd_char_array_Click);
            // 
            // cmd_char_array_rueckwaerts
            // 
            this.cmd_char_array_rueckwaerts.Location = new System.Drawing.Point(13, 398);
            this.cmd_char_array_rueckwaerts.Name = "cmd_char_array_rueckwaerts";
            this.cmd_char_array_rueckwaerts.Size = new System.Drawing.Size(107, 44);
            this.cmd_char_array_rueckwaerts.TabIndex = 8;
            this.cmd_char_array_rueckwaerts.Text = "Char-Array Rückwärts";
            this.cmd_char_array_rueckwaerts.UseVisualStyleBackColor = true;
            this.cmd_char_array_rueckwaerts.Click += new System.EventHandler(this.cmd_char_array_rueckwaerts_Click);
            // 
            // cmd_array_verweis
            // 
            this.cmd_array_verweis.Location = new System.Drawing.Point(157, 12);
            this.cmd_array_verweis.Name = "cmd_array_verweis";
            this.cmd_array_verweis.Size = new System.Drawing.Size(94, 47);
            this.cmd_array_verweis.TabIndex = 9;
            this.cmd_array_verweis.Text = "auf Array verweisen";
            this.cmd_array_verweis.UseVisualStyleBackColor = true;
            this.cmd_array_verweis.Click += new System.EventHandler(this.cmd_array_verweis_Click);
            // 
            // cmd_array_copy
            // 
            this.cmd_array_copy.Location = new System.Drawing.Point(157, 81);
            this.cmd_array_copy.Name = "cmd_array_copy";
            this.cmd_array_copy.Size = new System.Drawing.Size(94, 46);
            this.cmd_array_copy.TabIndex = 10;
            this.cmd_array_copy.Text = "Array kopieren";
            this.cmd_array_copy.UseVisualStyleBackColor = true;
            this.cmd_array_copy.Click += new System.EventHandler(this.cmd_array_copy_Click);
            // 
            // cmd_2d_array
            // 
            this.cmd_2d_array.Location = new System.Drawing.Point(157, 156);
            this.cmd_2d_array.Name = "cmd_2d_array";
            this.cmd_2d_array.Size = new System.Drawing.Size(94, 74);
            this.cmd_2d_array.TabIndex = 11;
            this.cmd_2d_array.Text = "Zweidimensionales Array";
            this.cmd_2d_array.UseVisualStyleBackColor = true;
            this.cmd_2d_array.Click += new System.EventHandler(this.cmd_2d_array_Click);
            // 
            // cmd_1_Zeichen_aus_2darray
            // 
            this.cmd_1_Zeichen_aus_2darray.Location = new System.Drawing.Point(157, 253);
            this.cmd_1_Zeichen_aus_2darray.Name = "cmd_1_Zeichen_aus_2darray";
            this.cmd_1_Zeichen_aus_2darray.Size = new System.Drawing.Size(94, 52);
            this.cmd_1_Zeichen_aus_2darray.TabIndex = 12;
            this.cmd_1_Zeichen_aus_2darray.Text = "ein Zeichen aus 2D-Array";
            this.cmd_1_Zeichen_aus_2darray.UseVisualStyleBackColor = true;
            this.cmd_1_Zeichen_aus_2darray.Click += new System.EventHandler(this.cmd_1_Zeichen_aus_2darray_Click);
            // 
            // Uebung_Array
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(673, 479);
            this.Controls.Add(this.cmd_1_Zeichen_aus_2darray);
            this.Controls.Add(this.cmd_2d_array);
            this.Controls.Add(this.cmd_array_copy);
            this.Controls.Add(this.cmd_array_verweis);
            this.Controls.Add(this.cmd_char_array_rueckwaerts);
            this.Controls.Add(this.cmd_char_array);
            this.Controls.Add(this.cmd_string_splitten);
            this.Controls.Add(this.cmd_string_arrays_3_Elementen);
            this.Controls.Add(this.cmd_double_array_mit_5_Elementen);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.cmd_Int10);
            this.Controls.Add(this.cmd_clear);
            this.Controls.Add(this.cmd_end);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Uebung_Array";
            this.Text = "j";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button cmd_end;
        private System.Windows.Forms.Button cmd_clear;
        private System.Windows.Forms.Button cmd_Int10;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Button cmd_double_array_mit_5_Elementen;
        private System.Windows.Forms.Button cmd_string_arrays_3_Elementen;
        private System.Windows.Forms.Button cmd_string_splitten;
        private System.Windows.Forms.Button cmd_char_array;
        private System.Windows.Forms.Button cmd_char_array_rueckwaerts;
        private System.Windows.Forms.Button cmd_array_verweis;
        private System.Windows.Forms.Button cmd_array_copy;
        private System.Windows.Forms.Button cmd_2d_array;
        private System.Windows.Forms.Button cmd_1_Zeichen_aus_2darray;
    }
}