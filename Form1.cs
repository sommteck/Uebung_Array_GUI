using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Übung_Array_GUI
{
    public partial class Uebung_Array : Form
    {
        private char LF = (char)10;

        public Uebung_Array()
        {
            InitializeComponent();
        }

        private void cmd_end_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void cmd_clear_Click(object sender, EventArgs e)
        {
            richTextBox1.Clear();
        }

        // Eindimensionaler Int-Array mit 10 Elementen
        private void cmd_Int10_Click(object sender, EventArgs e)
        {
            richTextBox1.Clear();

            int[] zahlen = new int[10]; // Deklaration des Arrays
            // Füüllen dieses Arrays mit Zufallsziffern
            Random zufafall = new Random();
            for (int i = 0; i < 10; i = i + 1)
                zahlen[i] = zufafall.Next(0, 10);
            // Ausgabe des Arrays
            for (int i = 0; i < 10; i = i +1)
                richTextBox1.Text += zahlen[i].ToString() + LF;
        }
        // Initialisierter Double-Array mit 5 Elementen
        private void cmd_double_array_mit_5_Elementen_Click(object sender, EventArgs e)
        {
            richTextBox1.Clear();

            double[] zahlen = { -3.4, 7.33, 0, 4.88, -77.1 };
            for (int i = 0; i < 5; i = i + 1)
                richTextBox1.Text += zahlen[i].ToString() + LF;
        }

        // Initialisieren eines String-Arrays mit 3 Elementen
        private void cmd_string_arrays_3_Elementen_Click(object sender, EventArgs e)
        {
            richTextBox1.Clear();

            string[] satz = new string[3];
            satz[0] = "Ich ";
            satz[1] = "hasse ";
            satz[2] = "C#!!! ";
            for (int i = 0; i < 3; i = i + 1)
                richTextBox1.Text += satz[i];
        }

        private void cmd_string_splitten_Click(object sender, EventArgs e)
        {
            richTextBox1.Clear();

            string adresse = "Hans;Dampf;Sternallee 1;A-Stadt";
            // String splitten und in ein Array schreiben
            string[] adressarray;
            adressarray = adresse.Split(';');
            richTextBox1.Text = adressarray[0] + " " + adressarray[1] + LF + adressarray[2] + LF + adressarray[3];
        }

        private void cmd_char_array_Click(object sender, EventArgs e)
        {
            richTextBox1.Clear();

            char[] chararray = { 'H', 'a', 'l', 'l', 'o' };
            for (int i = 0; i < chararray.Length; i = i + 1)
                richTextBox1.Text += chararray[i].ToString();
            richTextBox1.Text += LF;
        }

        private void cmd_char_array_rueckwaerts_Click(object sender, EventArgs e)
        {
            richTextBox1.Clear();

            char[] chararray = { 'H', 'a', 'l', 'l', 'o' };
            for (int i = chararray.Length - 1; i >= 0; i = i - 1)
                richTextBox1.Text += chararray[i].ToString();
            richTextBox1.Text += LF;
        }

        private void cmd_array_verweis_Click(object sender, EventArgs e)
        {
            int[] array1 = { 1, 2, 3 };
            int[] array2 = new int[3];
            // Verweis auf Array
            array2 = array1;
            // Array 1 manipulieren mit Array 2
            for (int i = 0; i < 3; i = i + 1)
                array2[i] *= 3;
            for (int i = 0; i < 3; i = i + 1)
                richTextBox1.Text += array1[i].ToString() + LF;
        }

        private void cmd_array_copy_Click(object sender, EventArgs e)
        {
            int[] array1 = { 1, 2, 3 };
            int[] array2 = new int[3];
            for (int i = 0; i < 3; i = i + 1)
                array2[i] = array1[i];
            for (int i = 0; i < 3; i = i + 1)
                array2[i] *= 3;
            for (int i = 0; i < 3; i = i + 1)
                richTextBox1.Text += array1[i].ToString() + LF;
            richTextBox1.Text += LF;
            for (int i = 0; i < 3; i = i + 1)
                richTextBox1.Text += array2[i].ToString() + LF;
        }

        private void cmd_2d_array_Click(object sender, EventArgs e)
        {
            // Zweidimensionales Array mit 3 Zeilen und 4 Spalten je Zeile
            int[,] array2d = new int[3, 4] { { 1, 2, 3, 4 }, { 5, 6, 7, 8 }, { 9, 10, 11, 12 } };
            for (int zeile = 0; zeile < 3; zeile = zeile + 1)
            {
                for (int spalte = 0; spalte < 4; spalte = spalte + 1)
                    richTextBox1.Text += array2d[zeile, spalte].ToString() + " ";
                richTextBox1.Text += LF;
            }
        }

        private void cmd_1_Zeichen_aus_2darray_Click(object sender, EventArgs e)
        {
            int[,] array2d = new int[3, 4] { { 1, 2, 3, 4 }, { 5, 6, 7, 8 }, { 9, 10, 11, 12 } };
            richTextBox1.Text = array2d[1, 2].ToString();
        }
    }
}